/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     The header definition.
 */

#ifndef _HEADER_P4_
#define _HEADER_P4_

header_type ethernet_t {
    fields {
        dst_addr : 48;
        src_addr : 48;
        ether_type : 16;
    }
}

header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        total_len : 16;
        identification : 16;
        flags : 3;
        frag_offset : 13;
        ttl : 8;
        proto : 8;
        checksum : 16;
        src_ip : 32;
        dst_ip: 32;
    }
}

header_type tcp_t {
    fields {
        src_port : 16;
        dst_port : 16;
        seq_no : 32;
        ack_no : 32;
        data_offset : 4;
        res : 3;
        ecn : 3;
        ctrl : 6;
        window : 16;
        checksum : 16;
        urgent_ptr : 16;
    }
}

header_type udp_t {
    fields {
        src_port : 16;
        dst_port : 16;
        hdr_length : 16;
        checksum : 16;
    }
}

header_type icmp_t {
    fields {
        msg_type : 8;
        code : 16;
        checksum : 16;
    }
}

header_type icmp_request_t {
    fields {
        identifier_be : 16;
        identifier_le : 16;
        seq_num_be : 16;
        seq_num_le : 16;
    }
}

header_type vlan_t {
    fields {
        pri     : 3;
        cfi     : 1;
        vlan_id : 12;
        ether_type : 16;
    }
}

header_type arp_t {
    fields {
        hw_type         : 16;
        proto_type      : 16;
        hw_addr_len     : 8;
        proto_addr_len  : 8;
        opcode          : 16;
        src_hw_addr     : 48;
        src_proto_addr  : 32;
        dst_hw_addr     : 48;
        dst_proto_addr  : 32;
    }
}

header_type newton_snapshot_t {
    fields {
        hash_value_a    : 32;
        state_value_a   : 32;        
        hash_value_b    : 32;
        state_value_b   : 32;
        global_result   : 32;
        ether_type      : 16;
    }
}

#endif /* End of _HEADER_P4_ */
