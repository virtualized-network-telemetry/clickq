/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     The program entrance for Newton.
 */


#include "common/target.p4"
#include "common/define.p4"
#include "common/header.p4"
#include "common/parser.p4"
#include "common/action.p4"

#include "clickq/clickq.p4"
#include "forward/forward.p4"

control ingress {
    forward();
}

control egress {
    clickq();
}

