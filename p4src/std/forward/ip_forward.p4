/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Forwarding module.
 */

#ifndef _IP_FORWARD_
#define _IP_FORWARD_

#include "forward_config.p4"
#include "forward_metadata.p4"


action set_egress_port(port) {
    modify_field(_egress_spec_, port);
    modify_field(global_field_set.egress_port, port);
}

table ip_switch {
    reads {
        forward_md.dst_ip : exact;
    }
    actions {
        set_egress_port;
    }
    size : 1024;
}

#endif /* End of _IP_FORWARD_ */