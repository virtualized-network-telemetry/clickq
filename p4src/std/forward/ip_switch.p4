/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Forwarding pakcets based on IP exact matching.
 */


table ip_switch {
    reads {
        forward_md.dst_ip : exact;
    }
    actions {
        set_egress_port;
    }
    size : 1024;
}