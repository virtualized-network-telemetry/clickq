/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ state bank module.
 */

#ifndef _CLOCKQ_STATE_BANK_
#define _CLOCKQ_STATE_BANK_

#define REG_WIDTH 32
#define REG_INSTANCE 65536

#define clickq_STATE_OP_NOOP   0
#define clickq_STATE_OP_ADD    1
#define clickq_STATE_OP_SET    2
#define clickq_STATE_OP_OR     3
#define clickq_STATE_OP_AND    4



#define STATE_BANK_ACTION(X, Y)			                                                                     \ 
register state_array_##X##_##Y  {                                                                            \
    width: REG_WIDTH;                                                                                        \
    instance_count: REG_INSTANCE;                                                                            \
}                                                                                                            \
action do_state_alu1_##X##_##Y (value) {                                                                     \
    register_read(result_set_##Y.state_value, state_array_##X##_##Y, result_set_##Y.hash_value);             \
    register_write(state_array_##X##_##Y, result_set_##Y.hash_value, result_set_##Y.state_value + value);    \
}                                                                                                            \
action do_state_alu2_##X##_##Y (value) {                                                                     \
    register_read(result_set_##Y.state_value, state_array_##X##_##Y, result_set_##Y.hash_value);             \
    register_write(state_array_##X##_##Y, result_set_##Y.hash_value, value);                                 \
}                                                                                                            \
action do_state_alu3_##X##_##Y (value) {                                                                     \
    register_read(result_set_##Y.state_value, state_array_##X##_##Y, result_set_##Y.hash_value);             \
    register_write(state_array_##X##_##Y, result_set_##Y.hash_value, result_set_##Y.state_value | value);    \
}                                                                                                            \
action do_state_alu4_##X##_##Y (value) {                                                                     \
    register_read(result_set_##Y.state_value, state_array_##X##_##Y, result_set_##Y.hash_value);             \
    register_write(state_array_##X##_##Y, result_set_##Y.hash_value, result_set_##Y.state_value | value);    \
}                                                                                                            \


action do_pass_result_a () {
    modify_field(result_set_a.state_value, result_set_a.hash_value);
}

action do_pass_result_b () {
    modify_field(result_set_b.state_value, result_set_b.hash_value);
}

#define STATE_BANK(X,Y)			              \
table state_bank_##X##_##Y {                  \
    reads { clickq_md.task_id: exact; }       \
    actions {                                 \
        do_state_alu1_##X##_##Y;              \
        do_state_alu2_##X##_##Y;              \        
        do_state_alu3_##X##_##Y;              \
        do_pass_result_##Y;}                  \
    size : 256;                               \
}                                             \               

#endif /* End of _CLOCKQ_STATE_BANK_ */
