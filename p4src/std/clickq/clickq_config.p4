/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Instance and declaration for ClickQ metadata.
 */

#ifndef _CLICKQ_CONFIG_
#define _CLICKQ_CONFIG_

#define TBL_SIZE_FIELD_SELECTION    1024
#define TBL_SIZE_HASH_CALCULATION   1024
#define TBL_SIZE_STATE_BANK         1024
#define TBL_SIZE_RESULT_PROCESS     1024

#endif /* End of _CLICKQ_CONFIG_ */