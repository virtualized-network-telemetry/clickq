/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ entrance.
 */


#ifndef _CLICKQ_ 
#define _CLICKQ_

#include "clickq_config.p4"
#include "clickq_metadata.p4"

#include "field_selection.p4"
#include "hash_calculation.p4"
#include "state_bank.p4"
#include "result_process.p4"


#define CLICKQ_STAGE_A(X)                      \
    control clickq_stage##X {                  \
        if (clickq_md.stop == 0) {             \
            apply(field_select_##X##_a);       \
            apply(hash_calculate_##X##_b);     \
            apply(state_bank_##X##_a);         \
            apply(result_process_##X##_b);     \
        }                                      \
    }


#define CLICKQ_STAGE_B(X)                      \
    control clickq_stage##X {                  \
        if (clickq_md.stop == 0) {             \
            apply(field_select_##X##_b);       \
            apply(hash_calculate_##X##_a);     \
            apply(state_bank_##X##_b);         \
            apply(result_process_##X##_a);     \
        }                                      \
    }



FIELD_SELECTION(1, a)
HASH_CALCULATION(1, b)
STATE_BANK_ACTION(1, a)			
STATE_BANK(1, a)        
RESULT_PROCESS(1, b) 
CLICKQ_STAGE_A(1)

FIELD_SELECTION(2, b)
HASH_CALCULATION(2, a)
STATE_BANK_ACTION(2, b)			
STATE_BANK(2, b)       
RESULT_PROCESS(2, a) 

CLICKQ_STAGE_B(2) 

FIELD_SELECTION(3, a)
HASH_CALCULATION(3, b)
STATE_BANK_ACTION(3, a)			
STATE_BANK(3, a)        
RESULT_PROCESS(3, b) 

CLICKQ_STAGE_A(3) 

FIELD_SELECTION(4, b)
HASH_CALCULATION(4, a)
STATE_BANK_ACTION(4, b)			
STATE_BANK(4, b)       
RESULT_PROCESS(4, a) 

CLICKQ_STAGE_B(4) 

FIELD_SELECTION(5, a)
HASH_CALCULATION(5, b)
STATE_BANK_ACTION(5, a)			
STATE_BANK(5, a)        
RESULT_PROCESS(5, b) 

CLICKQ_STAGE_A(5) 

FIELD_SELECTION(6, b)
HASH_CALCULATION(6, a)
STATE_BANK_ACTION(6, b)
STATE_BANK(6, b)       
RESULT_PROCESS(6, a) 

CLICKQ_STAGE_B(6)


FIELD_SELECTION(7, a)
HASH_CALCULATION(7, b)
STATE_BANK_ACTION(7, a)			
STATE_BANK(7, a)        
RESULT_PROCESS(7, b)
CLICKQ_STAGE_A(7) 

FIELD_SELECTION(8, b)
HASH_CALCULATION(8, a)
STATE_BANK_ACTION(8, b)
STATE_BANK(8, b)   
RESULT_PROCESS(8, a)
CLICKQ_STAGE_B(8)


action do_add_clickq_sp() {
    add_header(clickq_sp);
    modify_field(clickq_sp.hash_value_a, result_set_a.hash_value);
    modify_field(clickq_sp.hash_value_b, result_set_b.hash_value);
    modify_field(clickq_sp.state_value_a, result_set_a.state_value);
    modify_field(clickq_sp.state_value_b, result_set_a.state_value);
    modify_field(clickq_sp.global_result, clickq_md.global_result);
}

action do_mod_clickq_sp() {
    modify_field(clickq_sp.hash_value_a, result_set_a.hash_value);
    modify_field(clickq_sp.hash_value_b, result_set_b.hash_value);
    modify_field(clickq_sp.state_value_a, result_set_a.state_value);
    modify_field(clickq_sp.state_value_b, result_set_a.state_value);
    modify_field(clickq_sp.global_result, clickq_md.global_result);
}

action do_remove_clickq_sp() {
    modify_field(ethernet.ether_type, clickq_sp.ether_type);
    remove_header(clickq_sp);
}

table clickq_fin {
    reads {
        clickq_md.task_id : exact;
        clickq_md.stop : exact;
    }
    actions {
        do_add_clickq_sp;
        do_mod_clickq_sp;
        do_remove_clickq_sp;
    }
    size : 512;
}

action copy_ipv4() {
    modify_field(global_field_set.src_ip, ipv4.src_ip);
    modify_field(global_field_set.dst_ip, ipv4.dst_ip);
    modify_field(global_field_set.proto, ipv4.proto);
}

action copy_sp() {
    modify_field(result_set_a.hash_value, clickq_sp.hash_value_a);
    modify_field(result_set_b.hash_value, clickq_sp.hash_value_b);
    modify_field(result_set_a.state_value, clickq_sp.state_value_a);
    modify_field(result_set_b.state_value, clickq_sp.state_value_b);
    modify_field(clickq_md.global_result, clickq_sp.global_result);
}

action do_clickq_init_udp_with_sp (task_id) {
    modify_field(clickq_md.task_id, task_id);
    // copy_ipv4();
    copy_sp();
    /*
    modify_field(global_field_set.src_port, udp.src_port);
    modify_field(global_field_set.dst_port, udp.dst_port);
    modify_field(global_field_set.tcp_flag, 0);
    */
    modify_field(global_field_set.ingress_port, ig_intr_md.ingress_port);
}

action do_clickq_init_tcp_with_sp (task_id) {
    modify_field(clickq_md.task_id, task_id);
    // copy_ipv4();
    copy_sp();
    /*
    modify_field(global_field_set.src_port, tcp.src_port);
    modify_field(global_field_set.dst_port, tcp.dst_port);
    modify_field(global_field_set.tcp_flag, tcp.ctrl);
    */
    modify_field(global_field_set.ingress_port, ig_intr_md.ingress_port);
}

action do_clickq_init_udp_without_sp (task_id) {
    modify_field(clickq_md.task_id, task_id);
    /*
    copy_ipv4();
    modify_field(global_field_set.src_port, udp.src_port);
    modify_field(global_field_set.dst_port, udp.dst_port);
    modify_field(global_field_set.tcp_flag, 0);
    */
    modify_field(global_field_set.ingress_port, ig_intr_md.ingress_port);
}

action do_clickq_init_tcp_without_sp (task_id) {
    modify_field(clickq_md.task_id, task_id);
    /*
    copy_ipv4();
    modify_field(global_field_set.src_port, tcp.src_port);
    modify_field(global_field_set.dst_port, tcp.dst_port);
    modify_field(global_field_set.tcp_flag, tcp.ctrl);
    */
    modify_field(global_field_set.ingress_port, ig_intr_md.ingress_port);
}

action do_clickq_init (task_id, alu1, value1, alu2, value2, alu3, value3, 
                alu4, value4, alu5, value5, alu6, value6, alu7, value7, alu8, value8) {
    modify_field(clickq_md.task_id, task_id);
    modify_field(clickq_md.alu_type1, alu1);
    modify_field(clickq_md.alu_value1, value1);
    modify_field(clickq_md.alu_type2, alu2);
    modify_field(clickq_md.alu_value2, value3);
    modify_field(clickq_md.alu_type3, alu3);
    modify_field(clickq_md.alu_value3, value4);
    modify_field(clickq_md.alu_type4, alu4);
    modify_field(clickq_md.alu_value4, value5);
    modify_field(clickq_md.alu_type5, alu5);
    modify_field(clickq_md.alu_value5, value5);
    modify_field(clickq_md.alu_type6, alu6);
    modify_field(clickq_md.alu_value6, value6);
    modify_field(clickq_md.alu_type7, alu7);
    modify_field(clickq_md.alu_value7, value7);
    modify_field(clickq_md.alu_type8, alu8);
    modify_field(clickq_md.alu_value8, value8);
    /*
    copy_ipv4();
    modify_field(global_field_set.src_port, tcp.src_port);
    modify_field(global_field_set.dst_port, tcp.dst_port);
    modify_field(global_field_set.tcp_flag, tcp.ctrl);
    */
    // modify_field(global_field_set.ingress_port, ig_intr_md.ingress_port);
}



table clickq_init {
    reads {
        clickq_sp : valid;
        global_field_set.src_ip : ternary;
        global_field_set.dst_ip : ternary;
        global_field_set.proto : ternary;
        global_field_set.src_port : ternary;
        global_field_set.dst_port : ternary;
        global_field_set.tcp_flag : ternary;
    }
    actions {
        do_clickq_init;
        // do_clickq_init_tcp_with_sp;        
        // do_clickq_init_udp_without_sp;
        // do_clickq_init_tcp_without_sp;
    }
    size : 256;
}


control clickq {

    apply(clickq_init);

    clickq_stage1();
    clickq_stage2();
    clickq_stage3();
    clickq_stage4();
    clickq_stage5();
    clickq_stage6();
    clickq_stage7();
    clickq_stage8();

    apply(clickq_fin);
}

#endif
