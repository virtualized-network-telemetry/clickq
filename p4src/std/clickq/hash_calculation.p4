/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ Hash Calculation module.
 */


#ifndef _CLICKQ_HASH_CALCULATION_
#define _CLICKQ_HASH_CALCULATION_

#define HASH_BIT 32

field_list selected_field_list_a { selected_field_set_a; }
field_list selected_field_list_b { selected_field_set_b; }


field_list_calculation clickq_hash_cal_a {                   
    input { selected_field_list_a; }                          
    algorithm {                                             
        identity;                                           
        crc32;                                              
        crc_32q;                                            
        random;                                             
    }                                                       
    output_width: HASH_BIT;                                 
}
action do_clickq_hash_a(hash_off, hash_size) {                              
    modify_field_with_hash_based_offset(
        result_set_a.hash_value, hash_off, 
        clickq_hash_cal_a, hash_size);                  
}  

field_list_calculation clickq_hash_cal_b {                   
    input { selected_field_list_b; }                          
    algorithm {                                             
        identity;                                           
        crc32;                                              
        crc_32q;                                            
        random;                                             
    }                                                       
    output_width: HASH_BIT;                                 
}
action do_clickq_hash_b(hash_off, hash_size) {   
    modify_field_with_hash_based_offset(
        result_set_b.hash_value, hash_off, 
        clickq_hash_cal_a, hash_size);                  
}  


action do_set_selected_value_a() {
    modify_field(result_set_a.hash_value, 
                 result_set_a.selected_value);
}

action do_set_selected_value_b () {
    modify_field(result_set_b.hash_value, 
                 result_set_b.selected_value);
}


#define HASH_CALCULATION(X,Y)                   \
table hash_calculate_##X##_##Y {                 \
    reads { clickq_md.task_id : exact; }        \
    actions {                                   \
        do_set_selected_value_##Y;              \
        do_clickq_hash_##Y;                     \
    }                                           \
    size: 256;                                  \
}                                               

#endif /* End of  */
