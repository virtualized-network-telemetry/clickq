/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Instance and declaration for ClickQ metadata.
 */

#ifndef _CLICKQ_METADATA_
#define _CLICKQ_METADATA_

header_type clickq_field_set_t {
    fields {
        /* header fields */
        src_ip : 32;
        dst_ip : 32;

        src_port   : 16;
        dst_port   : 16;
        proto    : 8;
        tcp_flag : 8;

        /* path fields */
        ingress_port  : 16;
        egress_port  : 16;
        queue_id : 8;

        /* performance fields */
        queue_length : 16;

        /*  */
    }
}

metadata clickq_field_set_t global_field_set;

metadata clickq_field_set_t selected_field_set_a;
metadata clickq_field_set_t selected_field_set_b;

field_list global_field_list {
    global_field_set;
    result_set_a;
    result_set_b;
}


header_type clickq_result_set_t {
    fields {
        selected_value : 32;
        hash_value : 32;
        state_value : 32;
    }
}

metadata clickq_result_set_t result_set_a;
metadata clickq_result_set_t result_set_b;


/* 
 * Initialized at the start of the pipeline or parser.
 * Read-only for clickq modules.
 */
header_type clickq_md_t { 
    fields {
        task_id: 8;
        
        alu_type1 : 8;
        alu_type2 : 8;
        alu_type3 : 8;
        alu_type4 : 8;
        alu_type5 : 8;
        alu_type6 : 8;
        alu_type7 : 8;
        alu_type8 : 8;

        alu_value1 : 32;
        alu_value2 : 32;
        alu_value3 : 32;
        alu_value4 : 32;
        alu_value5 : 32;
        alu_value6 : 32;
        alu_value7 : 32;
        alu_value8 : 32;

        global_result : 16;

        stop : 8;
    }
}
metadata clickq_md_t clickq_md;



#endif /* End of _CLICKQ_METADATA_ */