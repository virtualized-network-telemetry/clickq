/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Parser.
 */

#ifndef _PARSER_
#define _PARSER_

parser start {
    return parse_ethernet;
}

header ethernet_t ethernet;
parser parse_ethernet {
    extract(ethernet);
    return select(latest.ether_type) {
        ETHERTYPE_IPV4 : parse_ipv4;  
        ETHERTYPE_VLAN : parse_vlan;  
        ETHERTYPE_VNT : parse_clickq;
        default: ingress;
    }
}

header clickq_snapshot_t clickq_sp;
parser parse_clickq {
    extract(clickq_sp);
    set_metadata(result_set_a.hash_value, clickq_sp.hash_value_a);
    set_metadata(result_set_b.hash_value, clickq_sp.hash_value_b);
    set_metadata(result_set_a.state_value, clickq_sp.state_value_a);
    set_metadata(result_set_b.state_value, clickq_sp.state_value_b);
    set_metadata(clickq_md.global_result, clickq_sp.global_result);
    return select(latest.ether_type) {
        ETHERTYPE_IPV4 : parse_ipv4;
        ETHERTYPE_ARP  : parse_arp;
        default: ingress;
    }
}

header arp_t arp;
parser parse_arp {
    extract(arp);
    set_metadata(forward_md.dst_ip, arp.dst_proto_addr);
    return ingress;
}

header vlan_t vlan;
parser parse_vlan {
    extract(vlan);
    return select(latest.ether_type) {
        ETHERTYPE_IPV4 : parse_ipv4;
        ETHERTYPE_ARP  : parse_arp;
        default: ingress;
    }
}

header ipv4_t ipv4;
parser parse_ipv4 {
    extract(ipv4);
    set_metadata(global_field_set.src_ip, ipv4.src_ip);
    set_metadata(global_field_set.dst_ip, ipv4.dst_ip);
    set_metadata(global_field_set.proto, ipv4.proto);
    set_metadata(forward_md.dst_ip, ipv4.dst_ip);
    return select(latest.proto) {
        IP_PROTOCOLS_TCP : parse_tcp;
        IP_PROTOCOLS_UDP : parse_udp;
        IP_PROTOCOLS_ICMP : parse_icmp;
        default: ingress;
    }
}

header tcp_t tcp;
parser parse_tcp {
    extract(tcp);
    set_metadata(global_field_set.src_port, tcp.src_port);
    set_metadata(global_field_set.dst_port, tcp.dst_port);
    set_metadata(global_field_set.tcp_flag, tcp.ctrl);
    return ingress;
}

header udp_t udp;
parser parse_udp {
    extract(udp);
    set_metadata(global_field_set.src_port, udp.src_port);
    set_metadata(global_field_set.dst_port, udp.dst_port);
    return ingress;
}

header icmp_t icmp;
parser parse_icmp {
    extract(icmp);
    return ingress;
}

#endif /* End of _PARSER_ */
