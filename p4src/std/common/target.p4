/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Target-specific values.
 */

#ifndef _TARGET_
#define _TARGET_

#define _egress_spec_   standard_metadata.egress_spec
#define _ingress_port_  standard_metadata.ingress_port 


#endif