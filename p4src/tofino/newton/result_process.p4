/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Newton result process module.
 */

#ifndef _RESULT_PROCESS_
#define _RESULT_PROCESS_

action report() {                                                       
    // generate_digest(0, global_field_list);                                  
    modify_field(newton_md.stop, 1);                                           
}                                                                               
action newton_stop() {                                                         
    modify_field(newton_md.stop, 1);                                           
}                                                                               
action newton_continue() {                                                     
    modify_field(newton_md.stop, 0);                                           
}                                                                               



action add_with_result_a () {                                                     
    modify_field(newton_md.global_result, 
                 result_set_a.state_value);                                           
}
action bit_and_a () {                                                     
    bit_and(newton_md.global_result, 
            newton_md.global_result, 
            result_set_a.state_value);                                           
}  
action bit_andca_a () {                                                     
    bit_and(newton_md.global_result, 
            newton_md.global_result, 
            result_set_a.state_value);                                           
}                               

action bit_andcb_a () {                                                     
    bit_and(newton_md.global_result, 
            newton_md.global_result, 
            result_set_a .state_value);                                           
}                               

action bit_nand_a () {                                                     
    bit_nand(newton_md.global_result, 
            newton_md.global_result, 
            result_set_a.state_value);                                           
} 

action bit_or_a () {
    bit_or(newton_md.global_result, 
           newton_md.global_result, 
           result_set_a.state_value);  
}             

action bit_nor_a () {                                               
    bit_or(newton_md.global_result, 
           newton_md.global_result, 
           result_set_a.state_value);  
}           

action bit_orca_a () {                                               
    bit_orca(newton_md.global_result, 
           newton_md.global_result, 
           result_set_a.state_value);  
}             

action bit_orcb_a () {                                               
    bit_orca(newton_md.global_result, 
           newton_md.global_result, 
           result_set_a.state_value);  
}  

action bit_not_a () {                                               
    bit_not(newton_md.global_result,
           result_set_a.state_value);  
}                      

action bit_xor_a () {                                               
    bit_xor(newton_md.global_result, 
            newton_md.global_result, 
            result_set_a.state_value);  
}                                           

action min_a () {
    min(newton_md.global_result, 
        newton_md.global_result, 
        result_set_a.state_value);  
}

action max_a () {
    max(newton_md.global_result, 
        newton_md.global_result, 
        result_set_a.state_value);  
}



action add_with_result_b () {                                                     
    modify_field(newton_md.global_result, 
                 result_set_b.state_value);                                           
}
action bit_and_b () {                                                     
    bit_and(newton_md.global_result, 
            newton_md.global_result, 
            result_set_b.state_value);                                           
}  
action bit_andca_b () {                                                     
    bit_and(newton_md.global_result, 
            newton_md.global_result, 
            result_set_b.state_value);                                           
}                               

action bit_andcb_b () {                                                     
    bit_and(newton_md.global_result, 
            newton_md.global_result, 
            result_set_b .state_value);                                           
}                               

action bit_nand_b () {                                                     
    bit_nand(newton_md.global_result, 
            newton_md.global_result, 
            result_set_b.state_value);                                           
} 

action bit_or_b () {
    bit_or(newton_md.global_result, 
           newton_md.global_result, 
           result_set_b.state_value);  
}             

action bit_nor_b () {                                               
    bit_or(newton_md.global_result, 
           newton_md.global_result, 
           result_set_b.state_value);  
}           

action bit_orca_b () {                                               
    bit_orca(newton_md.global_result, 
           newton_md.global_result, 
           result_set_b.state_value);  
}             

action bit_orcb_b () {                                               
    bit_orca(newton_md.global_result, 
           newton_md.global_result, 
           result_set_b.state_value);  
}  

action bit_not_b () {                                               
    bit_not(newton_md.global_result,
           result_set_b.state_value);  
}                      

action bit_xor_b () {                                               
    bit_xor(newton_md.global_result, 
            newton_md.global_result, 
            result_set_b.state_value);  
}                                           

action min_b () {
    min(newton_md.global_result, 
        newton_md.global_result, 
        result_set_b.state_value);  
}

action max_b () {
    max(newton_md.global_result, 
        newton_md.global_result, 
        result_set_b.state_value);  
}



action add_by_x(x) {                                                     
    add_to_field(newton_md.global_result, x);                                   
} 

action subtract_by_x(x) {                                                     
    add_to_field(newton_md.global_result, x);                                       
} 


@pragma stage 5
table result_process_0 {  
    reads {  
        newton_md.task_id : exact ;  
        newton_md.global_result : ternary;  
        result_set_b.state_value : ternary;  
    }   
    actions { 
        newton_report; 
        newton_stop;  
        newton_continue;  
        add_with_result_b; 
        bit_and_b;   
        bit_andca_b;  
        bit_andcb_b;    
        bit_or_b;        
        bit_orca_b;          
        bit_orcb_b;      
        bit_xor_b;
        min_b;             
        max_b;                 
        add_by_x;               
        subtract_by_x;                    
    }                                          
    default_action : newton_continue;             
    size : TBL_SIZE_RESULT_PROCESS;                               
}                                              

@pragma stage 6
table result_process_1 {  
    reads {  
        newton_md.task_id : exact ;  
        newton_md.global_result : ternary;  
        result_set_a.state_value : ternary;  
    }   
    actions { 
        newton_report; 
        newton_stop;  
        newton_continue;  
        add_with_result_a; 
        bit_and_a;   
        bit_andca_a;  
        bit_andcb_a;    
        bit_or_a;        
        bit_orca_a;          
        bit_orcb_a;      
        bit_xor_a;
        min_a;             
        max_a;                 
        add_by_x;               
        subtract_by_x;                    
    }                                          
    default_action : newton_continue;             
    size : TBL_SIZE_RESULT_PROCESS;                               
}                                              

@pragma stage 7
table result_process_2 {  
    reads {  
        newton_md.task_id : exact ;  
        newton_md.global_result : ternary;  
        result_set_b.state_value : ternary;  
    }   
    actions { 
        newton_report; 
        newton_stop;  
        newton_continue;  
        add_with_result_b; 
        bit_and_b;   
        bit_andca_b;  
        bit_andcb_b;    
        bit_or_b;        
        bit_orca_b;          
        bit_orcb_b;      
        bit_xor_b;
        min_b;             
        max_b;                 
        add_by_x;               
        subtract_by_x;                    
    }                                          
    default_action : newton_continue;             
    size : TBL_SIZE_RESULT_PROCESS;                               
}                                              

@pragma stage 8
table result_process_3 {  
    reads {  
        newton_md.task_id : exact ;  
        newton_md.global_result : ternary;  
        result_set_a.state_value : ternary;  
    }   
    actions { 
        newton_report; 
        newton_stop;  
        newton_continue;  
        add_with_result_a; 
        bit_and_a;   
        bit_andca_a;  
        bit_andcb_a;    
        bit_or_a;        
        bit_orca_a;          
        bit_orcb_a;      
        bit_xor_a;
        min_a;             
        max_a;                 
        add_by_x;               
        subtract_by_x;                    
    }                                          
    default_action : newton_continue;             
    size : TBL_SIZE_RESULT_PROCESS;                               
}                                              

@pragma stage 9
table result_process_4 {  
    reads {  
        newton_md.task_id : exact ;  
        newton_md.global_result : ternary;  
        result_set_b.state_value : ternary;  
    }   
    actions { 
        newton_report; 
        newton_stop;  
        newton_continue;  
        add_with_result_b; 
        bit_and_b;   
        bit_andca_b;  
        bit_andcb_b;    
        bit_or_b;        
        bit_orca_b;          
        bit_orcb_b;      
        bit_xor_b;
        min_b;             
        max_b;                 
        add_by_x;               
        subtract_by_x;                    
    }                                          
    default_action : newton_continue;             
    size : TBL_SIZE_RESULT_PROCESS;                               
}                                              

@pragma stage 10
table result_process_5 {  
    reads {  
        newton_md.task_id : exact ;  
        newton_md.global_result : ternary;  
        result_set_a.state_value : ternary;  
    }   
    actions { 
        newton_report; 
        newton_stop;  
        newton_continue;  
        add_with_result_a; 
        bit_and_a;   
        bit_andca_a;  
        bit_andcb_a;    
        bit_or_a;        
        bit_orca_a;          
        bit_orcb_a;      
        bit_xor_a;
        min_a;             
        max_a;                 
        add_by_x;               
        subtract_by_x;                    
    }                                          
    default_action : newton_continue;             
    size : TBL_SIZE_RESULT_PROCESS;                               
}                                              


#endif /* End of _CLICKQ_RESULT_PROCESS_ */