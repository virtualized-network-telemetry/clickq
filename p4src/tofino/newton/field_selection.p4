/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Newton field selection module.
 */

#ifndef _FIELD_SELECTION_
#define _FIELD_SELECTION_

/******* Field Selection *******/

action do_select_fields_a (ipv4_dst_ip_mask, ipv4_src_ip_mask , ipv4_proto_mask , flow_md_src_port_mask , flow_md_dst_port_mask , eg_intr_md_deq_qdepth_mask , ig_intr_md_ingress_port_mask , ig_intr_md_for_tm_ucast_egress_port_mask , ig_intr_md_ingress_mac_tstamp_mask ) {
    
    bit_and(key_set_a.ipv4_dst_ip,
        global_field_set.ipv4_dst_ip, ipv4_dst_ip_mask);
    bit_and(key_set_a.ipv4_src_ip,
        global_field_set.ipv4_src_ip, ipv4_src_ip_mask);
    bit_and(key_set_a.ipv4_proto,
        global_field_set.ipv4_proto, ipv4_proto_mask);
    bit_and(key_set_a.flow_md_src_port,
        global_field_set.flow_md_src_port, flow_md_src_port_mask);
    bit_and(key_set_a.flow_md_dst_port,
        global_field_set.flow_md_dst_port, flow_md_dst_port_mask);
    bit_and(key_set_a.eg_intr_md_deq_qdepth,
        global_field_set.eg_intr_md_deq_qdepth, eg_intr_md_deq_qdepth_mask);
    bit_and(key_set_a.ig_intr_md_ingress_port,
        global_field_set.ig_intr_md_ingress_port, ig_intr_md_ingress_port_mask);
    bit_and(key_set_a.ig_intr_md_for_tm_ucast_egress_port,
        global_field_set.ig_intr_md_for_tm_ucast_egress_port, ig_intr_md_for_tm_ucast_egress_port_mask);
    bit_and(key_set_a.ig_intr_md_ingress_mac_tstamp,
        global_field_set.ig_intr_md_ingress_mac_tstamp, ig_intr_md_ingress_mac_tstamp_mask);
}

action do_select_fields_b (ipv4_dst_ip_mask, ipv4_src_ip_mask , ipv4_proto_mask , flow_md_src_port_mask , flow_md_dst_port_mask , eg_intr_md_deq_qdepth_mask , ig_intr_md_ingress_port_mask , ig_intr_md_for_tm_ucast_egress_port_mask , ig_intr_md_ingress_mac_tstamp_mask ) {
    
    bit_and(key_set_b.ipv4_dst_ip,
        global_field_set.ipv4_dst_ip, ipv4_dst_ip_mask);
    bit_and(key_set_b.ipv4_src_ip,
        global_field_set.ipv4_src_ip, ipv4_src_ip_mask);
    bit_and(key_set_b.ipv4_proto,
        global_field_set.ipv4_proto, ipv4_proto_mask);
    bit_and(key_set_b.flow_md_src_port,
        global_field_set.flow_md_src_port, flow_md_src_port_mask);
    bit_and(key_set_b.flow_md_dst_port,
        global_field_set.flow_md_dst_port, flow_md_dst_port_mask);
    bit_and(key_set_b.eg_intr_md_deq_qdepth,
        global_field_set.eg_intr_md_deq_qdepth, eg_intr_md_deq_qdepth_mask);
    bit_and(key_set_b.ig_intr_md_ingress_port,
        global_field_set.ig_intr_md_ingress_port, ig_intr_md_ingress_port_mask);
    bit_and(key_set_b.ig_intr_md_for_tm_ucast_egress_port,
        global_field_set.ig_intr_md_for_tm_ucast_egress_port, ig_intr_md_for_tm_ucast_egress_port_mask);
    bit_and(key_set_b.ig_intr_md_ingress_mac_tstamp,
        global_field_set.ig_intr_md_ingress_mac_tstamp, ig_intr_md_ingress_mac_tstamp_mask);
}



action do_select_ipv4_dst_ip_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ipv4_dst_ip);
}

action do_select_ipv4_src_ip_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ipv4_src_ip);
}

action do_select_ipv4_proto_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ipv4_proto);
}

action do_select_flow_md_src_port_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.flow_md_src_port);
}

action do_select_flow_md_dst_port_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.flow_md_dst_port);
}

action do_select_eg_intr_md_deq_qdepth_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.eg_intr_md_deq_qdepth);
}

action do_select_ig_intr_md_ingress_port_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ig_intr_md_ingress_port);
}

action do_select_ig_intr_md_for_tm_ucast_egress_port_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ig_intr_md_for_tm_ucast_egress_port);
}

action do_select_ig_intr_md_ingress_mac_tstamp_a () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ig_intr_md_ingress_mac_tstamp);
}


action do_select_ipv4_dst_ip_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ipv4_dst_ip);
}

action do_select_ipv4_src_ip_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ipv4_src_ip);
}

action do_select_ipv4_proto_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ipv4_proto);
}

action do_select_flow_md_src_port_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.flow_md_src_port);
}

action do_select_flow_md_dst_port_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.flow_md_dst_port);
}

action do_select_eg_intr_md_deq_qdepth_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.eg_intr_md_deq_qdepth);
}

action do_select_ig_intr_md_ingress_port_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ig_intr_md_ingress_port);
}

action do_select_ig_intr_md_for_tm_ucast_egress_port_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ig_intr_md_for_tm_ucast_egress_port);
}

action do_select_ig_intr_md_ingress_mac_tstamp_b () {
    modify_field(result_set_.selected_value, 
                 global_field_set.ig_intr_md_ingress_mac_tstamp);
}





@pragma stage 5
table field_select_0_a {            
    reads { 
        newton_md.task_id: exact; 
    }
    actions {                                  
        do_select_fields_a;
        do_select_ipv4_dst_ip_a;
        do_select_ipv4_src_ip_a;
        do_select_ipv4_proto_a;
        do_select_flow_md_src_port_a;
        do_select_flow_md_dst_port_a;
        do_select_eg_intr_md_deq_qdepth_a;
        do_select_ig_intr_md_ingress_port_a;
        do_select_ig_intr_md_for_tm_ucast_egress_port_a;
        do_select_ig_intr_md_ingress_mac_tstamp_a;      
    }                                          
    size: TBL_SIZE_FIELD_SELECTION; 
} 

@pragma stage 6
table field_select_1_b {            
    reads { 
        newton_md.task_id: exact; 
    }
    actions {                                  
        do_select_fields_b;
        do_select_ipv4_dst_ip_b;
        do_select_ipv4_src_ip_b;
        do_select_ipv4_proto_b;
        do_select_flow_md_src_port_b;
        do_select_flow_md_dst_port_b;
        do_select_eg_intr_md_deq_qdepth_b;
        do_select_ig_intr_md_ingress_port_b;
        do_select_ig_intr_md_for_tm_ucast_egress_port_b;
        do_select_ig_intr_md_ingress_mac_tstamp_b;      
    }                                          
    size: TBL_SIZE_FIELD_SELECTION; 
} 

@pragma stage 7
table field_select_2_a {            
    reads { 
        newton_md.task_id: exact; 
    }
    actions {                                  
        do_select_fields_a;
        do_select_ipv4_dst_ip_a;
        do_select_ipv4_src_ip_a;
        do_select_ipv4_proto_a;
        do_select_flow_md_src_port_a;
        do_select_flow_md_dst_port_a;
        do_select_eg_intr_md_deq_qdepth_a;
        do_select_ig_intr_md_ingress_port_a;
        do_select_ig_intr_md_for_tm_ucast_egress_port_a;
        do_select_ig_intr_md_ingress_mac_tstamp_a;      
    }                                          
    size: TBL_SIZE_FIELD_SELECTION; 
} 

@pragma stage 8
table field_select_3_b {            
    reads { 
        newton_md.task_id: exact; 
    }
    actions {                                  
        do_select_fields_b;
        do_select_ipv4_dst_ip_b;
        do_select_ipv4_src_ip_b;
        do_select_ipv4_proto_b;
        do_select_flow_md_src_port_b;
        do_select_flow_md_dst_port_b;
        do_select_eg_intr_md_deq_qdepth_b;
        do_select_ig_intr_md_ingress_port_b;
        do_select_ig_intr_md_for_tm_ucast_egress_port_b;
        do_select_ig_intr_md_ingress_mac_tstamp_b;      
    }                                          
    size: TBL_SIZE_FIELD_SELECTION; 
} 

@pragma stage 9
table field_select_4_a {            
    reads { 
        newton_md.task_id: exact; 
    }
    actions {                                  
        do_select_fields_a;
        do_select_ipv4_dst_ip_a;
        do_select_ipv4_src_ip_a;
        do_select_ipv4_proto_a;
        do_select_flow_md_src_port_a;
        do_select_flow_md_dst_port_a;
        do_select_eg_intr_md_deq_qdepth_a;
        do_select_ig_intr_md_ingress_port_a;
        do_select_ig_intr_md_for_tm_ucast_egress_port_a;
        do_select_ig_intr_md_ingress_mac_tstamp_a;      
    }                                          
    size: TBL_SIZE_FIELD_SELECTION; 
} 

@pragma stage 10
table field_select_5_b {            
    reads { 
        newton_md.task_id: exact; 
    }
    actions {                                  
        do_select_fields_b;
        do_select_ipv4_dst_ip_b;
        do_select_ipv4_src_ip_b;
        do_select_ipv4_proto_b;
        do_select_flow_md_src_port_b;
        do_select_flow_md_dst_port_b;
        do_select_eg_intr_md_deq_qdepth_b;
        do_select_ig_intr_md_ingress_port_b;
        do_select_ig_intr_md_for_tm_ucast_egress_port_b;
        do_select_ig_intr_md_ingress_mac_tstamp_b;      
    }                                          
    size: TBL_SIZE_FIELD_SELECTION; 
} 



#endif /* End of _FIELD_SELECTION_ */