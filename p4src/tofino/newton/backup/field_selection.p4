/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ field selection module.
 */

#ifndef _FIELD_SELECTION_
#define _FIELD_SELECTION_

/******* Field Selection *******/
#define FIELD_SELECTION_ACTION(Y)                           \
action do_select_fields_##Y(src_ip_mask, dst_ip_mask,       \
    ip_proto_mask, src_port_mask, dst_port_mask,            \
    tcp_flag_mask, ingress_port_mask, egress_port_mask,     \ 
    queue_id_mask, queue_length_mask) {                     \
    bit_and(key_set_##Y.src_ip,                             \
        global_field_set.src_ip, src_ip_mask);              \
    bit_and(key_set_##Y.dst_ip,                             \
        global_field_set.dst_ip, dst_ip_mask);              \
    bit_and(key_set_##Y.proto,                              \
        global_field_set.proto, ip_proto_mask);             \
    bit_and(key_set_##Y.src_port,                           \ 
        global_field_set.src_port, src_port_mask);          \
    bit_and(key_set_##Y.dst_port,                           \ 
        global_field_set.dst_port, dst_port_mask);          \
    bit_and(key_set_##Y.tcp_flag,                           \ 
        global_field_set.tcp_flag, tcp_flag_mask);          \
    bit_and(key_set_##Y.ingress_port,                       \
        global_field_set.ingress_port, ingress_port_mask);  \
    bit_and(key_set_##Y.egress_port,                        \
        global_field_set.egress_port, egress_port_mask);    \
    bit_and(key_set_##Y.queue_id,                           \ 
        global_field_set.queue_id, queue_id_mask);          \
    bit_and(key_set_##Y.queue_length,                       \ 
        global_field_set.queue_length, queue_length_mask);  \
}


action do_select_src_ip_a () {
    modify_field(result_set_a.selected_value, global_field_set.src_ip);
}

action do_select_src_ip_b () {
    modify_field(result_set_b.selected_value, global_field_set.src_ip);
}

action do_select_dst_ip_a () {
    modify_field(result_set_a.selected_value, global_field_set.dst_ip);
}

action do_select_dst_ip_b () {
    modify_field(result_set_b.selected_value, global_field_set.dst_ip);
}

action do_select_proto_a () {
    modify_field(result_set_a.selected_value, global_field_set.src_port);
}

action do_select_proto_b () {
    modify_field(result_set_b.selected_value, global_field_set.proto);
}

action do_select_src_port_a () {
    modify_field(result_set_a.selected_value, global_field_set.proto);
}

action do_select_src_port_b () {
    modify_field(result_set_b.selected_value, global_field_set.src_port);
}

action do_select_dst_port_a () {
    modify_field(result_set_a.selected_value, global_field_set.dst_port);
}

action do_select_dst_port_b () {
    modify_field(result_set_b.selected_value, global_field_set.dst_port);
}

action do_select_tcp_flag_a () {
    modify_field(result_set_a.selected_value, global_field_set.tcp_flag);
}

action do_select_tcp_flag_b () {
    modify_field(result_set_b.selected_value, global_field_set.tcp_flag);
}


FIELD_SELECTION_ACTION(a)
FIELD_SELECTION_ACTION(b)

#define FIELD_SELECTION(X, Y)                  \
table field_select_##X##_##Y {                 \
    reads { newton_md.task_id: exact; }        \
    actions {                                  \
        do_select_fields_##Y;                  \
        do_select_dst_port_##Y;                \
        do_select_src_port_##Y;                \
        do_select_tcp_flag_##Y;                \
        do_select_proto_##Y;                   \
        do_select_src_ip_##Y;                  \
        do_select_dst_ip_##Y;                  \
        do_select_tcp_flag_##Y;                \
    }                                          \
    size: TBL_SIZE_FIELD_SELECTION;            \
}                                          

#endif
