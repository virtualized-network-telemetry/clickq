/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ state bank module.
 */

#ifndef _CLOCKQ_STATE_BANK_
#define _CLOCKQ_STATE_BANK_

#define REG_WIDTH 32
#define REG_INSTANCE 65536

#define clickq_STATE_OP_NOOP   0
#define clickq_STATE_OP_ADD    1
#define clickq_STATE_OP_SET    2
#define clickq_STATE_OP_OR     3
#define clickq_STATE_OP_AND    4



#define STATE_BANK_ACTION(X, Y)			                                        \ 
register state_array_##X##_##Y  {                                               \
    width: REG_WIDTH;                                                           \
    instance_count: REG_INSTANCE;                                               \
}                                                                               \
blackbox stateful_alu state_alu1_##X##_##Y {                                    \
    reg : state_array_##X##_##Y;                                                \
    condition_lo : clickq_md.alu_type##X == clickq_STATE_OP_ADD;                \
    condition_hi : clickq_md.alu_type##X == clickq_STATE_OP_SET;                \
    update_lo_1_predicate: condition_lo;                                        \
    update_lo_1_value: register_lo + clickq_md.alu_value##X;                    \
    update_lo_2_predicate: condition_hi;                                        \
    update_lo_2_value: clickq_md.alu_value##X;                                  \
    output_value: register_lo;                                                  \
    output_dst: result_set_##Y.state_value;                                     \
}                                                                               \
action do_state_alu1_##X##_##Y  () {                                            \
    state_alu1_##X##_##Y.execute_stateful_alu(result_set_##Y.hash_value);       \
}                                                                               \ 
blackbox stateful_alu state_alu2_##X##_##Y {                                    \
    reg : state_array_##X##_##Y;                                                \
    condition_lo : clickq_md.alu_type##X == clickq_STATE_OP_OR;                 \
    condition_hi : clickq_md.alu_type##X == clickq_STATE_OP_AND;                \
    update_lo_1_predicate: condition_lo;                                        \
    update_lo_1_value: register_lo | clickq_md.alu_value##X;                    \
    update_lo_2_predicate: condition_hi;                                        \
    update_lo_2_value: register_lo & clickq_md.alu_value##X;                    \
    output_value: register_lo;                                                  \
    output_dst: result_set_##Y.state_value;                                     \
}                                                                               \
action do_state_alu2_##X##_##Y () {                                             \
    state_alu2_##X##_##Y.execute_stateful_alu(result_set_##Y.hash_value);       \
}                                                                 


action do_pass_result_a () {
    modify_field(result_set_a.state_value, result_set_a.hash_value);
}

action do_pass_result_b () {
    modify_field(result_set_b.state_value, result_set_b.hash_value);
}

#define STATE_BANK(X,Y)			              \
table state_bank_##X##_##Y {                  \
    reads { clickq_md.task_id: exact; }       \
    actions {                                 \
        do_state_alu1_##X##_##Y;              \
        do_state_alu2_##X##_##Y;              \
        do_pass_result_##Y;}                  \
    size : 256;                               \
}                                             \               

#endif /* End of _CLOCKQ_STATE_BANK_ */
