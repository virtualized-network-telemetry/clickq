/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ Hash Calculation module.
 */


#ifndef _NEWTON_HASH_CALCULATION_
#define _NEWTON_HASH_CALCULATION_

#define HASH_BIT 32
#define HASH_BLOCK 256

field_list key_list_a { key_set_a; }
field_list key_list_b { key_set_b; }

field_list_calculation hash_cal_a {
    input {
        key_list_a; 
    }
    algorithm {                                   
        identity; 
        crc32;
        crc_32q;
        random;
    }
    output_width: HASH_BIT;
}
action do_hash_a () {     
    modify_field_with_hash_based_offset(
        result_set_a.hash_value, 0, 
        newton_hash_cal_a, HASH_BLOCK);
}  

field_list_calculation hash_cal_b {                   
    input { 
        key_list_b; 
    }                          
    algorithm {                                
        identity; 
        crc32; 
        crc_32q; 
        random;
    } 
    output_width: HASH_BIT; 
}
action do_hash_b () {                              
    modify_field_with_hash_based_offset(
        result_set_b.hash_value, 0, 
        hash_cal_a, HASH_BLOCK);
}  

action do_set_key_a () {
    modify_field(result_set_a.hash_value, 
                 result_set_a.selected_value);
}

action do_set_key_b () {
    modify_field(result_set_b.hash_value, 
                 result_set_b.selected_value);
}

#define HASH_CALCULATION(X, Y)                  \
table hash_calculate_##X##_##Y {                \
    reads { newton_md.task_id : exact; }        \
    actions {                                   \
        do_set_key_##Y;                         \
        do_newton_hash_##Y;                     \
    }                                           \
    size: 256;                                  \
}                                               

#endif
