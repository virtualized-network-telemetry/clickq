#ifndef _CLICKQ_RESULT_PROCESS_
#define _CLICKQ_RESULT_PROCESS_

action clickq_report() {                                                       
    // generate_digest(0, global_field_list);                                  
    modify_field(clickq_md.stop, 1);                                           
}                                                                               
action clickq_stop() {                                                         
    modify_field(clickq_md.stop, 1);                                           
}                                                                               
action clickq_continue() {                                                     
    modify_field(clickq_md.stop, 0);                                           
}                                                                               

action add_with_result_a() {                                                     
    modify_field(clickq_md.global_result, 
                 result_set_a.state_value);                                           
}                               

action add_with_result_b() {                                                     
    modify_field(clickq_md.global_result, 
                 result_set_b.state_value);                                           
}

action bit_and_a() {                                                     
    bit_and(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);                                           
}          

action bit_and_b() {                                                     
    bit_and(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
} 

action bit_andca_a() {                                                     
    bit_and(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);                                           
}                               

action bit_andca_b() {                                                     
    bit_and(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}              

action bit_andcb_a() {                                                     
    bit_and(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);                                           
}                               

action bit_andcb_b() {                                                     
    bit_and(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}              

action bit_nand_a() {                                                     
    bit_nand(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);                                           
}                               

action bit_nand_b() {                                                     
    bit_nand(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}              

action bit_or_a() {                                               
    bit_or(clickq_md.global_result, 
           clickq_md.global_result, 
           result_set_a.state_value);  
}             
                  
action bit_or_b() {                                                     
    bit_nor(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}              

action bit_nor_a() {                                               
    bit_or(clickq_md.global_result, 
           clickq_md.global_result, 
           result_set_a.state_value);  
}           

action bit_nor_b() {                                                     
    bit_nor(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}    

action bit_orca_a() {                                               
    bit_orca(clickq_md.global_result, 
           clickq_md.global_result, 
           result_set_a.state_value);  
}             
                  
action bit_orca_b() {                                                     
    bit_orca(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}    

action bit_orcb_a() {                                               
    bit_orca(clickq_md.global_result, 
           clickq_md.global_result, 
           result_set_a.state_value);  
}             
                  
action bit_orcb_b() {                                                     
    bit_orca(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}    

action bit_not_a() {                                               
    bit_not(clickq_md.global_result,
           result_set_a.state_value);  
}                      

action bit_not_b() {                                                     
    bit_not(clickq_md.global_result, 
            result_set_b.state_value);                                           
}    

action bit_xor_a() {                                               
    bit_xor(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);  
}                                           

action bit_xor_b() {                                                     
    bit_xor(clickq_md.global_result, 
            clickq_md.global_result,
            result_set_b.state_value);                                           
}
action min_a () {
    min(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);  
}

action min_b () {
    min(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_b.state_value);  
}

action max_a () {
    max(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_a.state_value);  
}

action max_b () {
    max(clickq_md.global_result, 
            clickq_md.global_result, 
            result_set_b.state_value);  
}

action add_by_x(x) {                                                     
    add_to_field(clickq_md.global_result, x);                                   
} 

action subtract_by_x(x) {                                                     
    add_to_field(clickq_md.global_result, x);                                       
} 

#define RESULT_PROCESS(X,Y)                     \
table result_process_##X##_##Y {                 \
    reads {                                     \
        clickq_md.task_id: exact ;                 \
        clickq_md.global_result : ternary;         \
        result_set_##Y.state_value : ternary;   \   
    }                                           \
    actions {                                   \
        clickq_report;                             \
        clickq_stop;                               \
        clickq_continue;                           \
        add_with_result_##Y;                    \
        bit_and_##Y;                            \
        bit_andca_##Y;                          \
        bit_andcb_##Y;                          \
        bit_or_##Y;                             \
        bit_orca_##Y;                           \
        bit_orcb_##Y;                           \
        bit_xor_##Y;                            \
        min_##Y;                                \ 
        max_##Y;                                \ 
        add_by_x;                               \
        subtract_by_x;                          \
    }                                           \
    default_action : clickq_continue;              \
    size : 2048;                                \
}                                               \

#endif /* End of _CLICKQ_RESULT_PROCESS_ */
