/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Instance and declaration for ClickQ metadata.
 */

#ifndef _CLICKQ_CONFIG_
#define _CLICKQ_CONFIG_

#define TBL_SIZE_FIELD_SELECTION    256
#define TBL_SIZE_HASH_CALCULATION   256
#define TBL_SIZE_STATE_BANK         256
#define TBL_SIZE_RESULT_PROCESS     256

#endif /* End of _CLICKQ_CONFIG_ */