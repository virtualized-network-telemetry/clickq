/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Newton state bank module.
 */

#ifndef _STATE_BANK_
#define _STATE_BANK_

#define NEWTON_STATE_OP_NOOP   0
#define NEWTON_STATE_OP_ADD    1
#define NEWTON_STATE_OP_SET    2
#define NEWTON_STATE_OP_OR     3
#define NEWTON_STATE_OP_AND    4

#define REG_WIDTH         32
#define REG_INSTANCE_NUM  65536


action do_pass_result_a () {
    modify_field(result_set_a.state_value, result_set_a.hash_value);
}

action do_pass_result_b () {
    modify_field(result_set_b.state_value, result_set_b.hash_value);
}



register state_array_0  { 
    width: REG_WIDTH;                                                          
    instance_count: REG_INSTANCE_NUM;
}         
blackbox stateful_alu state_alu1_0 {                                   
    reg : state_array_0;                                                
    condition_lo : newton_md.alu_type_a == newton_STATE_OP_ADD;
    condition_hi : newton_md.alu_type_a == newton_STATE_OP_SET;
    update_lo_1_predicate: condition_lo; 
    update_lo_1_value: register_lo + newton_md.alu_value_a;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: newton_md.alu_value_a;
    output_value: register_lo;                                 
    output_dst: result_set_a.state_value;
}
action do_state_alu1_0 () {
    state_alu1_0.execute_stateful_alu(result_set_a.hash_value);
} 
blackbox stateful_alu stateful_alu state_alu2_0 {
    reg : state_array_0;
    condition_lo : newton_md.alu_type_a == newton_STATE_OP_OR;
    condition_hi : newton_md.alu_type_a == newton_STATE_OP_AND;
    update_lo_1_predicate: condition_lo;
    update_lo_1_value: register_lo | newton_md.alu_value_a;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: register_lo & newton_md.alu_value_a;
    output_value: register_lo;
    output_dst: result_set_a.state_value;  
} 
action do_state_alu2_0 () { 
    state_alu2_0.execute_stateful_alu(result_set_a.hash_value);
} 

@pragma stage 5
table state_bank_0 {                 
    reads { 
        newton_md.task_id: exact; 
    }      
    actions {                                
        do_state_alu1_0;             
        do_state_alu2_0;             
        do_pass_result_a;
    }                 
    size : TBL_SIZE_STATE_BANK;                              
}                

register state_array_1  { 
    width: REG_WIDTH;                                                          
    instance_count: REG_INSTANCE_NUM;
}         
blackbox stateful_alu state_alu1_1 {                                   
    reg : state_array_1;                                                
    condition_lo : newton_md.alu_type_b == newton_STATE_OP_ADD;
    condition_hi : newton_md.alu_type_b == newton_STATE_OP_SET;
    update_lo_1_predicate: condition_lo; 
    update_lo_1_value: register_lo + newton_md.alu_value_b;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: newton_md.alu_value_b;
    output_value: register_lo;                                 
    output_dst: result_set_b.state_value;
}
action do_state_alu1_1 () {
    state_alu1_1.execute_stateful_alu(result_set_b.hash_value);
} 
blackbox stateful_alu stateful_alu state_alu2_1 {
    reg : state_array_1;
    condition_lo : newton_md.alu_type_b == newton_STATE_OP_OR;
    condition_hi : newton_md.alu_type_b == newton_STATE_OP_AND;
    update_lo_1_predicate: condition_lo;
    update_lo_1_value: register_lo | newton_md.alu_value_b;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: register_lo & newton_md.alu_value_b;
    output_value: register_lo;
    output_dst: result_set_b.state_value;  
} 
action do_state_alu2_1 () { 
    state_alu2_1.execute_stateful_alu(result_set_b.hash_value);
} 

@pragma stage 6
table state_bank_1 {                 
    reads { 
        newton_md.task_id: exact; 
    }      
    actions {                                
        do_state_alu1_1;             
        do_state_alu2_1;             
        do_pass_result_b;
    }                 
    size : TBL_SIZE_STATE_BANK;                              
}                

register state_array_2  { 
    width: REG_WIDTH;                                                          
    instance_count: REG_INSTANCE_NUM;
}         
blackbox stateful_alu state_alu1_2 {                                   
    reg : state_array_2;                                                
    condition_lo : newton_md.alu_type_a == newton_STATE_OP_ADD;
    condition_hi : newton_md.alu_type_a == newton_STATE_OP_SET;
    update_lo_1_predicate: condition_lo; 
    update_lo_1_value: register_lo + newton_md.alu_value_a;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: newton_md.alu_value_a;
    output_value: register_lo;                                 
    output_dst: result_set_a.state_value;
}
action do_state_alu1_2 () {
    state_alu1_2.execute_stateful_alu(result_set_a.hash_value);
} 
blackbox stateful_alu stateful_alu state_alu2_2 {
    reg : state_array_2;
    condition_lo : newton_md.alu_type_a == newton_STATE_OP_OR;
    condition_hi : newton_md.alu_type_a == newton_STATE_OP_AND;
    update_lo_1_predicate: condition_lo;
    update_lo_1_value: register_lo | newton_md.alu_value_a;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: register_lo & newton_md.alu_value_a;
    output_value: register_lo;
    output_dst: result_set_a.state_value;  
} 
action do_state_alu2_2 () { 
    state_alu2_2.execute_stateful_alu(result_set_a.hash_value);
} 

@pragma stage 7
table state_bank_2 {                 
    reads { 
        newton_md.task_id: exact; 
    }      
    actions {                                
        do_state_alu1_2;             
        do_state_alu2_2;             
        do_pass_result_a;
    }                 
    size : TBL_SIZE_STATE_BANK;                              
}                

register state_array_3  { 
    width: REG_WIDTH;                                                          
    instance_count: REG_INSTANCE_NUM;
}         
blackbox stateful_alu state_alu1_3 {                                   
    reg : state_array_3;                                                
    condition_lo : newton_md.alu_type_b == newton_STATE_OP_ADD;
    condition_hi : newton_md.alu_type_b == newton_STATE_OP_SET;
    update_lo_1_predicate: condition_lo; 
    update_lo_1_value: register_lo + newton_md.alu_value_b;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: newton_md.alu_value_b;
    output_value: register_lo;                                 
    output_dst: result_set_b.state_value;
}
action do_state_alu1_3 () {
    state_alu1_3.execute_stateful_alu(result_set_b.hash_value);
} 
blackbox stateful_alu stateful_alu state_alu2_3 {
    reg : state_array_3;
    condition_lo : newton_md.alu_type_b == newton_STATE_OP_OR;
    condition_hi : newton_md.alu_type_b == newton_STATE_OP_AND;
    update_lo_1_predicate: condition_lo;
    update_lo_1_value: register_lo | newton_md.alu_value_b;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: register_lo & newton_md.alu_value_b;
    output_value: register_lo;
    output_dst: result_set_b.state_value;  
} 
action do_state_alu2_3 () { 
    state_alu2_3.execute_stateful_alu(result_set_b.hash_value);
} 

@pragma stage 8
table state_bank_3 {                 
    reads { 
        newton_md.task_id: exact; 
    }      
    actions {                                
        do_state_alu1_3;             
        do_state_alu2_3;             
        do_pass_result_b;
    }                 
    size : TBL_SIZE_STATE_BANK;                              
}                

register state_array_4  { 
    width: REG_WIDTH;                                                          
    instance_count: REG_INSTANCE_NUM;
}         
blackbox stateful_alu state_alu1_4 {                                   
    reg : state_array_4;                                                
    condition_lo : newton_md.alu_type_a == newton_STATE_OP_ADD;
    condition_hi : newton_md.alu_type_a == newton_STATE_OP_SET;
    update_lo_1_predicate: condition_lo; 
    update_lo_1_value: register_lo + newton_md.alu_value_a;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: newton_md.alu_value_a;
    output_value: register_lo;                                 
    output_dst: result_set_a.state_value;
}
action do_state_alu1_4 () {
    state_alu1_4.execute_stateful_alu(result_set_a.hash_value);
} 
blackbox stateful_alu stateful_alu state_alu2_4 {
    reg : state_array_4;
    condition_lo : newton_md.alu_type_a == newton_STATE_OP_OR;
    condition_hi : newton_md.alu_type_a == newton_STATE_OP_AND;
    update_lo_1_predicate: condition_lo;
    update_lo_1_value: register_lo | newton_md.alu_value_a;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: register_lo & newton_md.alu_value_a;
    output_value: register_lo;
    output_dst: result_set_a.state_value;  
} 
action do_state_alu2_4 () { 
    state_alu2_4.execute_stateful_alu(result_set_a.hash_value);
} 

@pragma stage 9
table state_bank_4 {                 
    reads { 
        newton_md.task_id: exact; 
    }      
    actions {                                
        do_state_alu1_4;             
        do_state_alu2_4;             
        do_pass_result_a;
    }                 
    size : TBL_SIZE_STATE_BANK;                              
}                

register state_array_5  { 
    width: REG_WIDTH;                                                          
    instance_count: REG_INSTANCE_NUM;
}         
blackbox stateful_alu state_alu1_5 {                                   
    reg : state_array_5;                                                
    condition_lo : newton_md.alu_type_b == newton_STATE_OP_ADD;
    condition_hi : newton_md.alu_type_b == newton_STATE_OP_SET;
    update_lo_1_predicate: condition_lo; 
    update_lo_1_value: register_lo + newton_md.alu_value_b;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: newton_md.alu_value_b;
    output_value: register_lo;                                 
    output_dst: result_set_b.state_value;
}
action do_state_alu1_5 () {
    state_alu1_5.execute_stateful_alu(result_set_b.hash_value);
} 
blackbox stateful_alu stateful_alu state_alu2_5 {
    reg : state_array_5;
    condition_lo : newton_md.alu_type_b == newton_STATE_OP_OR;
    condition_hi : newton_md.alu_type_b == newton_STATE_OP_AND;
    update_lo_1_predicate: condition_lo;
    update_lo_1_value: register_lo | newton_md.alu_value_b;
    update_lo_2_predicate: condition_hi;
    update_lo_2_value: register_lo & newton_md.alu_value_b;
    output_value: register_lo;
    output_dst: result_set_b.state_value;  
} 
action do_state_alu2_5 () { 
    state_alu2_5.execute_stateful_alu(result_set_b.hash_value);
} 

@pragma stage 10
table state_bank_5 {                 
    reads { 
        newton_md.task_id: exact; 
    }      
    actions {                                
        do_state_alu1_5;             
        do_state_alu2_5;             
        do_pass_result_b;
    }                 
    size : TBL_SIZE_STATE_BANK;                              
}                


#endif /* End of _STATE_BANK_ */