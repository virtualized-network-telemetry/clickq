/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     Instance and declaration for Newton metadata.
 */

#ifndef _NEWTON_METADATA_
#define _NEWTON_METADATA_

header_type field_set_t {
    fields {
        ipv4_dst_ip : 32;
        ipv4_src_ip : 32;
        ipv4_proto : 32;
        flow_md_src_port : 32;
        flow_md_dst_port : 32;
        eg_intr_md_deq_qdepth : 32;
        ig_intr_md_ingress_port : 32;
        ig_intr_md_for_tm_ucast_egress_port : 32;
        ig_intr_md_ingress_mac_tstamp : 32;
        }
}

metadata field_set_t global_field_set;

metadata field_set_t key_set_a;
metadata field_set_t key_set_b;

field_list global_field_list {
    global_field_set;
    result_set_a;
    result_set_b;
}

header_type result_set_t {
    fields {
        selected_value : 32;
        hash_value : 32;
        state_value : 32;
    }
}

metadata result_set_t result_set_a;
metadata result_set_t result_set_b;


/* 
 * Initialized at the start of the pipeline or parser.
 * Read-only for clickq modules.
 */
header_type newton_md_t { 
    fields {
        task_id: 8;
        
        alu_type_a : 8;
        alu_value_a : 32;
        alu_type_b : 8;
        alu_value_b : 32;

        global_result : 16;
        
        stop : 8;
    }
}
metadata newton_md_t newton_md;



#endif /* End of _CLICKQ_METADATA_ */