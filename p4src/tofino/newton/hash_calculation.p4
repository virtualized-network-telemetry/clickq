/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ Hash Calculation module.
 */


#ifndef _HASH_CALCULATION_
#define _HASH_CALCULATION_

#define HASH_BIT    32
#define BLOCK_SIZE  256


field_list key_list_a { key_set_a; }
field_list key_list_b { key_set_b; }


field_list_calculation hash_cal_a {
    input {
        key_list_a;
    }
    algorithm {      
        identity;
        crc32;
        crc_32q;
        random;
        }
    output_width: HASH_BIT;
}
action do_hash_a () { 
    modify_field_with_hash_based_offset(
        result_set_a.hash_value, 0, 
        hash_cal_a, HASH_BLOCK);
}
action do_set_key_a () {
    modify_field(result_set_a.hash_value, 
                 result_set_a.selected_value);
}

field_list_calculation hash_cal_b {
    input {
        key_list_b;
    }
    algorithm {      
        identity;
        crc32;
        crc_32q;
        random;
        }
    output_width: HASH_BIT;
}
action do_hash_b () { 
    modify_field_with_hash_based_offset(
        result_set_b.hash_value, 0, 
        hash_cal_b, HASH_BLOCK);
}
action do_set_key_b () {
    modify_field(result_set_b.hash_value, 
                 result_set_b.selected_value);
}




@pragma stage 5
table hash_calculate_0_b {                
    reads { 
        newton_md.task_id : exact; 
    }        
    actions {
        do_set_key_b;
        do_newton_hash_b;
    }
    size: TBL_SIZE_HASH_CALCULATION;
}                                               

@pragma stage 6
table hash_calculate_1_a {                
    reads { 
        newton_md.task_id : exact; 
    }        
    actions {
        do_set_key_a;
        do_newton_hash_a;
    }
    size: TBL_SIZE_HASH_CALCULATION;
}                                               

@pragma stage 7
table hash_calculate_2_b {                
    reads { 
        newton_md.task_id : exact; 
    }        
    actions {
        do_set_key_b;
        do_newton_hash_b;
    }
    size: TBL_SIZE_HASH_CALCULATION;
}                                               

@pragma stage 8
table hash_calculate_3_a {                
    reads { 
        newton_md.task_id : exact; 
    }        
    actions {
        do_set_key_a;
        do_newton_hash_a;
    }
    size: TBL_SIZE_HASH_CALCULATION;
}                                               

@pragma stage 9
table hash_calculate_4_b {                
    reads { 
        newton_md.task_id : exact; 
    }        
    actions {
        do_set_key_b;
        do_newton_hash_b;
    }
    size: TBL_SIZE_HASH_CALCULATION;
}                                               

@pragma stage 10
table hash_calculate_5_a {                
    reads { 
        newton_md.task_id : exact; 
    }        
    actions {
        do_set_key_a;
        do_newton_hash_a;
    }
    size: TBL_SIZE_HASH_CALCULATION;
}                                               


#endif /* End of HASH_CALCULATION */