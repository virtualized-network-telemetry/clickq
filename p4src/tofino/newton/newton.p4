/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     ClickQ entrance.
 */


#ifndef _NEWTON_P4_ 
#define _NEWTON_P4_

#include "newton_config.p4"
#include "newton_metadata.p4"

#include "field_selection.p4"
#include "hash_calculation.p4"
#include "state_bank.p4"
#include "result_process.p4"

control newton_ingress_stages {
}

control newton_egress_stages {
    if (newton.stop == 0) {
        apply(field_select_0);      
        apply(hash_calculate_0);  
        apply(state_bank_0);  
        apply(result_process_0);  
    }
    if (newton.stop == 0) {
        apply(field_select_1);      
        apply(hash_calculate_1);  
        apply(state_bank_1);  
        apply(result_process_1);  
    }
    if (newton.stop == 0) {
        apply(field_select_2);      
        apply(hash_calculate_2);  
        apply(state_bank_2);  
        apply(result_process_2);  
    }
    if (newton.stop == 0) {
        apply(field_select_3);      
        apply(hash_calculate_3);  
        apply(state_bank_3);  
        apply(result_process_3);  
    }
    if (newton.stop == 0) {
        apply(field_select_4);      
        apply(hash_calculate_4);  
        apply(state_bank_4);  
        apply(result_process_4);  
    }
    if (newton.stop == 0) {
        apply(field_select_5);      
        apply(hash_calculate_5);  
        apply(state_bank_5);  
        apply(result_process_5);  
    }
}


table newton_fin {
    reads {
        newton_md.task_id : exact;
        newton_md.stop : exact;
    }
    actions {
        do_add_newton_sp;
        do_mod_newton_sp;
        do_remove_newton_sp;
    }
    size : 512;
}

action do_newton_init (task_id, alu_a, value_a, alu_b, value_b) {
    modify_field(newton_md.task_id, task_id);
    modify_field(newton_md.alu_type_a, alu_a);
    modify_field(newton_md.alu_value_a, value_a);
    modify_field(newton_md.alu_type_b, alu_b);
    modify_field(newton_md.alu_value_b, value_b);
    modify_field(global_field_set.ipv4_dst_ip, ipv4.dst_ip);
    modify_field(global_field_set.ipv4_src_ip, ipv4.src_ip);
    modify_field(global_field_set.ipv4_proto, ipv4.proto);
    modify_field(global_field_set.flow_md_src_port, flow_md.src_port);
    modify_field(global_field_set.flow_md_dst_port, flow_md.dst_port);
    modify_field(global_field_set.eg_intr_md_deq_qdepth, eg_intr_md.deq_qdepth);
    modify_field(global_field_set.ig_intr_md_ingress_port, ig_intr_md.ingress_port);
    modify_field(global_field_set.ig_intr_md_for_tm_ucast_egress_port, ig_intr_md_for_tm.ucast_egress_port);
    modify_field(global_field_set.ig_intr_md_ingress_mac_tstamp, ig_intr_md.ingress_mac_tstamp);
}

table newton_init {
    reads {
        newton_sp           : valid;
        flow_md.src_ip      : ternary;
        flow_md.dst_ip      : ternary;
        flow_md.proto       : ternary;
        flow_md.src_port    : ternary;
        flow_md.dst_port    : ternary;
        tcp.ctrl            : ternary;
    }
    actions {
        do_newton_init;
    }
    size : 256;
}

control newton_ingress {
    apply(newton_init);
    newton_ingress_stages();
}

control newton_ingress {
    newton_egress_stages();
    apply(newton_fin);
}

#endif /* _NEWTON_P4_ */