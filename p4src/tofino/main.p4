/**
 * Authors:
 *     Yu Zhou, Tsinghua University, y-zhou16@mails.tsinghua.edu.cn
 * File Description:
 *     The program entrance for ClickQ.
 */


#include "common/tofino.p4"
#include "common/define.p4"
#include "common/header.p4"
#include "common/parser.p4"
#include "common/action.p4"

#include "newton/newton.p4"
#include "forward/ip_forward.p4"

control ingress {
    forward();
    newton_ingress();
}

control egress {
    newton_egress();
}
