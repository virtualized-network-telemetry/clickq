import os, sys
import json
from jinja2 import Template

def p4_template_compile(context):
    reload(sys)
    sys.setdefaultencoding('utf8')

    src_dir = os.path.dirname(__file__)
    
    template_dir = os.path.join(src_dir, "template")
    newton_dir = os.path.join(src_dir, "newton")

    file_list = [
        'field_selection.p4',
        'hash_calculation.p4',
        'newton_metadata.p4',
        'newton.p4',
        'result_process.p4',
        'state_bank.p4'
    ]
    for file_name in file_list:
        template_file = os.path.join(template_dir, file_name + '.jinja')
        with open(template_file) as f:
            template = Template(f.read())
            p4 = template.render(newton = newton)
            dst_file_name = os.path.join(newton_dir, file_name)
            src = open(dst_file_name, 'w')
            src.write(p4)
            src.close()

if __name__ == "__main__":
    newton = {
        'symbols' : ['a', 'b'],
    }
    with open("code_config.json") as f:
        config = json.load(f)
        newton.update(config)
    modules = []
    for i in range(newton["stage_num"]):
        m = {
            'id' : i,
            'stage' : i + 5,
            'is_ingress' : 0,
            'field_select_symbol' : newton['symbols'][i % 2],
            'hash_calculate_symbol' : newton['symbols'][(i + 1) % 2],
            'state_bank_symbol' : newton['symbols'][i % 2],
            'result_process_symbol' : newton['symbols'][(i + 1) % 2]
        }
        modules.append(m)
    newton['modules'] = modules

    p4_template_compile(newton)
    
