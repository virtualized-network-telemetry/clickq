#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
import json

from runtime import *

g_mode = sys.argv[1]

def handle_tm_forward(switch_runtime, entries):
    for entry in entries:
        action_name = entry['action_name']
        action_parameters = (g_port_dict[entry['action_parameters'][0]], )
        match_parameters = (g_port_dict[entry['match_parameters'][0]], )
        switch_runtime.add_table_entry('tm_forward', action_name, match_parameters, action_parameters)

def handle_tm_forward_model(switch_runtime):
    for i in range(8):
        action_parameters = (i + 1, )
        match_parameters = (i, )
        switch_runtime.add_table_entry('tm_forward', 'do_tm_forward', match_parameters, action_parameters)

if __name__ == "__main__":
    switch_runtime = None
    if g_mode == 'MODEL':
        switch_runtime = SwitchRuntime('profile.json', TOFINO_MODEL_MODE)
    else:
        switch_runtime = SwitchRuntime('profile.json')
    switch_runtime.start()

    with open("pd.json") as pd_f:
        pd = json.load(pd_f)

        for table in pd:
            if table['name'] == 'tm_forward' and g_mode != 'MODEL':
                handle_tm_forward(switch_runtime, table['entries'])

    switch_runtime.set_default_entry("monitor_initialize", "do_monitor_initialize", ())
    switch_runtime.set_default_entry("monitor_ts_initialize", "do_monitor_ts_initialize", ())

    switch_runtime.add_table_entry("monitor_pre_check", "monitor_nop", (0,0,0,0,0,0,0,0,0), ())
    switch_runtime.add_table_entry("monitor_check", "monitor_check_hit", (0,0,0,0,0,0,0,0,0), ())
    switch_runtime.set_default_entry("monitor_report", "do_monitor_report", (1, ))

    if g_mode == 'MODEL':
        handle_tm_forward_model(switch_runtime)
    
    switch_runtime.stop()